# Changelog
All notable changes to this project will be documented in this file.

## [0.1.0] - 2021-21-30
### Added
Support for rolling tables directly from compendia.
### Changed
Compatibility witht the new Foundry version (0.9.0).
### Removed

## [0.0.1] - 2021-05-31
### Added
Release
### Changed
### Removed
