Hooks.on("renderRollTableDirectory", (rolltables, html, data) => {
    AddRollButton(rolltables)
});

Hooks.on("renderCompendium", (app, html, data) => {
    AddRollButton(app)
});

function AddRollButton(app) {
    const items = app.element.find(".directory-item.rolltable");
    items.each((k) => {
        let rollIcon = document.createElement("a");
        rollIcon.classList.add("roll-table");
        rollIcon.setAttribute("data-action", "roll-table");
        rollIcon.setAttribute("title", game.i18n.localize("RollTableFromSidebar.Roll"));
        // Create roll icon
        let die = document.createElement("i");
        die.classList.add("fas");
        die.classList.add("fa-dice");
        rollIcon.appendChild(die);
        items[k].appendChild(rollIcon);
        rollIcon.addEventListener("click", RollTableFromSidebar);
    })
}

async function RollTableFromSidebar(event) {
    const tableId = event.currentTarget.parentElement.dataset["documentId"];
    let table = game.tables.get(tableId);
    if (!table) {
        const directory = event.currentTarget.closest("div.compendium.directory");
        const metadata = game.packs.get(directory.dataset["pack"]).metadata;
        const compendium = new Compendium(new CompendiumCollection(metadata));
        table = await compendium.collection.getDocument(tableId);
    }
    table.draw();
}
