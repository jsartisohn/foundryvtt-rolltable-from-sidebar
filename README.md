# Rolltable From Sidebar for Foundry VTT
This module allows rollling rollable tables (world and compendium) directly from the sidebar.

Once the module is enabled, you will see a dice icon next to the rollable table name. Just click on it.

![](preview.gif)
